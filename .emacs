(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
;(package-initialize)

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
(package-refresh-contents)
(package-install 'use-package))
;; This makes it so it only loads my org file for my init
(org-babel-load-file (expand-file-name "~/.emacs.d/init.org"))


;; Commenting out everything below, just to be sure I have a backup

;
;(require 'package)
;(package-initialize)
;(add-to-list 'package-archives 
;    '("MELPA" .
;      "http://melpa.org/packages/"))
;
;;;;;;;;;dirvish;;;;;;;;;;;;;;;
;(use-package dirvish
;  :init
;  (dirvish-override-dired-mode)
;  :custom
;  ;; Go back home? Just press `bh'
;  (dirvish-bookmark-entries
;   '(("h" "~/"                          "Home")
;     ("d" "~/Downloads/"                "Downloads")
;     ("m" "/mnt/"                       "Drives")
;     ("t" "~/.local/share/Trash/files/" "TrashCan")))
;  ;; (dirvish-header-line-format '(:left (path) :right (free-space)))
;  (dirvish-mode-line-format ; it's ok to place string inside
;   '(:left (sort file-time " " file-size symlink) :right (omit yank index)))
;  ;; Don't worry, Dirvish is still performant even you enable all these attributes
;  (dirvish-attributes '(all-the-icons file-size collapse subtree-state vc-state git-msg))
;  ;; Maybe the icons are too big to your eyes
;  ;; (dirvish-all-the-icons-height 0.8)
;  ;; In case you want the details at startup like `dired'
;  ;; (dirvish-hide-details nil)
;  :config
;  (dirvish-peek-mode)
;  ;; Dired options are respected except a few exceptions, see *In relation to Dired* section above
;  (setq dired-dwim-target t)
;  (setq delete-by-moving-to-trash t)
;  ;; Enable mouse drag-and-drop files to other applications
;  (setq dired-mouse-drag-files t)                   ; added in Emacs 29
;  (setq mouse-drag-and-drop-region-cross-program t) ; added in Emacs 29
;  ;; Make sure to use the long name of flags when exists
;  ;; eg. use "--almost-all" instead of "-A"
;  ;; Otherwise some commands won't work properly
;  (setq dired-listing-switches
;        "-l --almost-all --human-readable --time-style=long-iso --group-directories-first --no-group")
;  :bind
;  ;; Bind `dirvish|dirvish-side|dirvish-dwim' as you see fit
;  (("C-c f" . dirvish-fd)
;   :map dired-mode-map ; Dirvish respects all the keybindings in this map
;   ("<prior>" . dired-up-directory)
;   ;; ("j" . dired-next-line)
;   ;; ("k" . dired-previous-line)
;   ;; ("l" . dired-find-file)
;   ;; ("i" . wdired-change-to-wdired-mode)
;   ;; ("." . dired-omit-mode)
;   ("b"   . dirvish-bookmark-jump)
;   ("f"   . dirvish-file-info-menu)
;   ("y"   . dirvish-yank-menu)
;   ("N"   . dirvish-narrow)
;   ("^"   . dirvish-history-last)
;   ("s"   . dirvish-quicksort) ; remapped `dired-sort-toggle-or-edit'
;   ("?"   . dirvish-dispatch)  ; remapped `dired-summary'
;   ("TAB" . dirvish-subtree-toggle)
;   ("SPC" . dirvish-history-jump)
;   ("M-n" . dirvish-history-go-forward)
;   ("M-p" . dirvish-history-go-backward)
;   ("M-l" . dirvish-ls-switches-menu)
;   ("M-m" . dirvish-mark-menu)
;   ("M-f" . dirvish-toggle-fullscreen)
;   ("M-s" . dirvish-setup-menu)
;   ("M-e" . dirvish-emerge-menu)
;   ("M-j" . dirvish-fd-jump)))
;;;;;;;;;;yasnippet;;;;;;;;;;;;;
;(yas-global-mode 1)
;(setq yas-indent-line nil)
;;;;;;;;;;;;;;;;;;;;;;;; ORG ROAM ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(use-package org-roam
;	     :ensure t
;	     :custom
;	     (org-roam-directory (file-truename "/home/hank/Documents/roam-notes"))
;	     :bind (("C-c n l" . org-roam-buffer-toggle)
;		    ("C-c n f" . org-roam-node-find)
;		    ("C-c n g" . org-roam-graph)
;		    ("C-c n i" . org-roam-node-insert)
;		    ("C-c n c" . org-roam-capture)
;		    ;;Daily
;		    ("C-c n j" . org-roam-dailies-capture-today))
;	     :config
;	     (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
;	     (org-roam-db-autosync-mode)
;	     (require 'org-roam-protocol))
;
;;;;;;;;;;;;;;;;;ORG Misc;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; spell checking on by default, requires hunspell and the hunspell-en_us dictionary
;;(add-hook 'org-mode-hook 'turn-on-flyspell)
;; correct flyspell words with C-.
;(flyspell-mode)
;(add-hook 'text-mode-hook 'flyspell-mode)
;(add-hook 'prog-mode-hook 'flyspell-prog-mode)
;;(add-hook 'java-mode-hook 'flyspell-mode)
;;(require 'flymake)
;;(add-hook 'java-mode-hook 'flymake-mode-on)
;
;(setq org-src-preserve-indentation t)
;
;; org-fragtog makes it so you can automatically preview LaTeX without having to do anything
;(add-hook 'org-mode-hook 'org-fragtog-mode)
;;(plist-put org-format-latex-options :scale 2)
;;(after! org (plist-put org-format-latex-options :scale 1.5))
;(font-lock-add-keywords 'org-mode
;			'(("^ *\\([-]\\) "
;			   (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
;
;
;(font-lock-add-keywords 'org-mode
;			'(("^ *\\([--]\\) "
;			   (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "➡"))))))
;
;;; Superstar replaces org-bullets
;(require 'org-superstar)
;(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))
;
;;(use-package org-bullets
;;    :hook (org-mode . org-bullets-mode))
;;;hides markup emphasis like *dd*
;(setq org-hide-emphasis-markers t)
;
;;;; NEED TO FIGURE OUT HOW TO CHANGE DEFAULTS
;;(setq org-clocktable-defaults 
;
;;(global-org-modern-mode)
;
;;;;;; Agenda
;(setq org-agenda-files "~/Documents/Agenda/agenda.org")
;
;
;;;;;;;;;;;;;;;;;;;hyperbole mode;;;;;;;;;;;;;;;;;
;(hyperbole-mode 1)
;
;;;;;;;;;;;;;;;;;;ELFEED and ELFEED-DASHBOARD;;;;;;;;;;;;;
;(setq elfeed-feeds
;      '(("https://ooohspooky.libsyn.com/rss" AdamKnox)
;	("https://godotengine.org/rss.xml" Godot)
;	("https://stackabuse.com/rss/" StackAbuse)
;	("https://opensource.com/feed" OpenSource)
;	("https://daverupert.com/atom.xml" DaveRupert)
;	("https://feed.podbean.com/mateitsasurething/feed.xml" Wolfe)
;	("https://www.omnycontent.com/d/playlist/e73c998e-6e60-432f-8610-ae210140c5b1/0c6fc944-eaa5-4022-8e77-ae3300346d76/45a735b6-e1b8-49f9-a595-ae3300346d84/podcast.rss" Scrubs)))
;;(use-package elfeed-org
;;  :ensure t
;;  :config
;;  (elfeed-org)
;;  (setq rmh-elfeed-org-files (list "~/elfeed-dashboard.org")))
;
;;; Load elfeed-org
;(require 'elfeed-org)
;
;;; Initialize elfeed-org
;;; This hooks up elfeed-org to read the configuration when elfeed
;;; is started with =M-x elfeed=
;(elfeed-org)
;
;;; Optionally specify a number of files containing elfeed
;;; configuration. If not set then the location below is used.
;;; Note: The customize interface is also supported.
;(setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed-dashboard.org"))
;
;
;(use-package elfeed-dashboard
;  :ensure t
;  :config
;  (setq elfeed-dashboard-file "~/elfeed-dashboard.org")
;  ;; update feed counts on elfeed-quit
;  (advice-add 'elfeed-search-quit-window :after #'elfeed-dashboard-update-links))
;;(elfeed-dashboard-mode)
;(require 'elfeed-goodies)
;(elfeed-goodies/setup)
;;(setq elfeed-dashboard-file "~/.emacs.d/elfeed-dashboard.org")
;(global-set-key (kbd "C-x w") 'elfeed)
;
;;;; g = refresh view
;;;; G = fetch feed updates
;;;; s = Update search filter
;;;; c = clear search filter
;
;;;;;;;;;;;;;;;;;;;EMMS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://www.draketo.de/software/emacs-tipps#elfeed-emms
;(use-package emms
;  :ensure t
;  :config)
;(global-set-key (kbd "C-x p") 'ivy-emms)
;(require 'emms-setup)
;(require 'emms-player-mplayer)
;(emms-all)
;(setq emms-player-list '(
;			 emms-player-mpg321
;			 emms-player-ogg123
;			 emms-player-mplayer))
;(defun emms-player-mplayer-volume(amount)
;  (process-send-string
;   emms-player-simple-process-name
;   (format "volume %d\n" amount)))
;(setq emms-volume-change-function 'emms-player-mplayer-volume)
;(emms-default-players)
;;(emms-add-directory-tree "~/Music/")
;;(emms-add-directory-tree "~/Downloads/")
;
;;;;;;;;;;;;;;;;;;;;;;;;;meow mode;;;;;;;;;;;;;;;;;;;;;;;;
;(defun meow-setup ()
;  (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
;  (meow-motion-overwrite-define-key
;   '("k" . meow-next)
;   '("j" . meow-prev)
;   '("<escape>" . ignore))
;  (meow-leader-define-key
;   ;; SPC j/k will run the original command in MOTION state.
;   '("j" . "H-j")
;   '("k" . "H-k")
;   ;; Use SPC (0-9) for digit arguments.
;   '("1" . meow-digit-argument)
;   '("2" . meow-digit-argument)
;   '("3" . meow-digit-argument)
;   '("4" . meow-digit-argument)
;   '("5" . meow-digit-argument)
;   '("6" . meow-digit-argument)
;   '("7" . meow-digit-argument)
;   '("8" . meow-digit-argument)
;   '("9" . meow-digit-argument)
;   '("0" . meow-digit-argument)
;   '("/" . meow-keypad-describe-key)
;   '("?" . meow-cheatsheet))
;  (meow-normal-define-key
;   '("0" . meow-expand-0)
;   '("9" . meow-expand-9)
;   '("8" . meow-expand-8)
;   '("7" . meow-expand-7)
;   '("6" . meow-expand-6)
;   '("5" . meow-expand-5)
;   '("4" . meow-expand-4)
;   '("3" . meow-expand-3)
;   '("2" . meow-expand-2)
;   '("1" . meow-expand-1)
;   '("-" . negative-argument)
;   '(";" . meow-reverse)
;   '("," . meow-inner-of-thing)
;   '("." . meow-bounds-of-thing)
;   '("[" . meow-beginning-of-thing)
;   '("]" . meow-end-of-thing)
;   '("a" . meow-append)
;   '("A" . meow-open-below)
;   '("b" . meow-back-word)
;   '("B" . meow-back-symbol)
;   '("c" . meow-change)
;   '("d" . meow-delete)
;   '("D" . meow-backward-delete)
;   '("e" . meow-next-word)
;   '("E" . meow-next-symbol)
;   '("f" . meow-find)
;   '("g" . meow-cancel-selection)
;   '("G" . meow-grab)
;   '("h" . meow-left)
;   '("H" . meow-left-expand)
;   '("i" . meow-insert)
;   '("I" . meow-open-above)
;   '("j" . meow-next)
;   '("J" . meow-next-expand)
;   '("k" . meow-prev)
;   '("K" . meow-prev-expand)
;   '("l" . meow-right)
;   '("L" . meow-right-expand)
;   '("m" . meow-join)
;   '("n" . meow-search)
;   '("o" . meow-block)
;   '("O" . meow-to-block)
;   '("p" . meow-yank)
;   '("q" . meow-quit)
;   '("Q" . meow-goto-line)
;   '("r" . meow-replace)
;   '("R" . meow-swap-grab)
;   '("s" . meow-kill)
;   '("t" . meow-till)
;   '("u" . meow-undo)
;   '("U" . meow-undo-in-selection)
;   '("v" . meow-visit)
;   '("w" . meow-mark-word)
;   '("W" . meow-mark-symbol)
;   '("x" . meow-line)
;   '("X" . meow-goto-line)
;   '("y" . meow-save)
;   '("Y" . meow-sync-grab)
;   '("z" . meow-pop-selection)
;   '("'" . repeat)
;   '("<escape>" . ignore)))
;
;(setq meow-cursor-type-normal 'hbar)
;(setq meow-cursor-type-insert 'box)
;
;(require 'meow)
;(meow-setup)
;(meow-global-mode 1)
;
;;;;;;;;;;;;;;;;;;;Cell Mode;;;;;;;;;;;;;;;;;;;;
;;(add-to-list 'load-path "/home/hank/.emacs.d/cell-mode")
;;(require 'cell-mode)
;
;;;;;;;;;;;;;;;;;;;;mu4e;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu4e")
;;(require 'mu4e)
;
;;(mu4e-alert-set-default-style 'libnotify)
;;(add-hook 'after-init-hook 'mu4e-alert-enable-notifications)
;;(mu4e-alert-notifications)
;
;;;;;;;;;;;;;;;;;;;;;Windmove uses shift and arrow to change window;;;;;;
;(when (fboundp 'windmove-default-keybindings)
;  (windmove-default-keybindings))
;
;;;;;;;;;;;;;;;;;;;kills scratch buffer on launch;;;;;;;;;;;;;;;;;;;;
;(kill-buffer "*scratch*")
;;(setq inhibit-splash-screen t)
;(setq inhibit-startup-screen t)
;(scroll-bar-mode -1)
;(menu-bar-mode -1)
;(tool-bar-mode -1)
;;Undo is C-/!!!!!!!!!!!!!!!
;;(add-hook 'window-setup-hook 'toggle-frame-fullscreen t)
;;(set-frame-parameter nil 'fullscreen)
;;(kill-buffer "*Messages*")
;;(kill-buffer "*GNU Emacs*")
;
;;;;;;;;;;;;;;;;;;;;Diary(reminders/calendar);;;;;;;;;;;;;;;;;;;;;;;;;
;;replace - with underscore  but underscore isn't working on ppkb
;;https://psachin.gitlab.io/emacs-diary-desktop-notification.html
;;minibuffer notifications
;;(setq initial-buffer-choice "~/.emacs.d/diary")
;
;;(setq initial-buffer-choice 'diary-fancy-display-mode)
;
;;desktop notifications
;(require 'notifications)
;
;(defcustom appt-notification-bus :session
;  "dbus notifications"
;  :version "29.05"
;  :group 'appt-notification
;  :type '(choice (const :tag "Session bus" :session) string))
;
;(defun user/appt-display (min-to-app new-time msg)
;  "sends notifications"
;  (notifications-notify :bus appt-notification-bus
;			:title (format "Appointment in %s minutes" min-to-app)
;			:body (format "%s" msg)
;			:replaces-id nil
;			:app-icon notifications-application-icon
;			:timeout 8000
;			:desktop-entry "diary"))
;
;(setq appt-disp-window-function (function user/appt-display))
;
;(setq-default diary-file "/home/hank/.emacs.d/diary"
;	      appt-display-format 'window
;	      appt-display-duration 60
;	      appt-audible t
;	      appt-display-interval 15
;	      appt-message-warning-time 15
;	      display-time-24hr-format t
;	      display-time-day-and-date t)
;
;(appt-activate t)
;
;;imports ical file, takes a long time since my ical is huge from old events
;;(icalendar-import-file "~/.local/share/evolution/calendar/system/calendar.ics"
;;		       "~/.emacs.d/diary")
;
;
;;;;;displays emojis for weather
;(emojify-mode 1)
;(emojify-mode-line-mode 1)
;
;;;;;;;;;;;;;;;;;;;;;modeline edits;;;;;;;;;;
;(display-time-mode t)
;;(line-number-mode 1)
;;(setq mode-line-modes nil)
;;(setq mode-line-buffer-identification nil)
;;(setq mode-line-frame-identification nil)
;
;
;;;;;;;;;;;;;;;;;;;;dashboard;;;;;;;;;;;;;;;;
;;; Set the title
;(require 'dashboard)
;;(dashboard-setup-startup-hook)
;
;(setq dashboard-week-agenda t)
;(setq dashboard-banner-logo-title "I'm using Linux. A library that Emacs uses to communicate with Intel hardware.") 
;;(setq dashboard-startup-banner "/home/hank/Downloads/rsz_emacs-colorful.png")
;(setq dashboard-startup-banner "/home/hank/Pictures/emacs-colorful.png")
;;(setq dashboard-startup-banner 'official)
;(setq dashboard-set-heading-icons t)
;(setq dashboard-set-file-icons t)
;(setq dashboard-set-navigator t)
;(setq dashboard-center-content t)
;;; To disable shortcut "jump" indicators for each section, set
;(setq dashboard-show-shortcuts nil)
;
;(dashboard-setup-startup-hook)
;
;(auto-image-file-mode 1)
;
;(use-package page-break-lines
;  :ensure t
;  :demand t)
;
;;(global-page-break-lines-mode 1)
;
;;;;;;;;;;;;;;;;;;;;;svg stuff;;;;;;;;;;;;;;;
;(defconst date-re "[0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\}")
;(defconst time-re "[0-9]\\{2\\}:[0-9]\\{2\\}")
;(defconst day-re "[A-Za-z]\\{3\\}")
;(defconst day-time-re (format "\\(%s\\)? ?\\(%s\\)?" day-re time-re))
;
;(defun svg-progress-percent (value)
;  (svg-image (svg-lib-concat
;              (svg-lib-progress-bar (/ (string-to-number value) 100.0)
;                                nil :margin 0 :stroke 2 :radius 3 :padding 2 :width 11)
;              (svg-lib-tag (concat value "%")
;                           nil :stroke 0 :margin 0)) :ascent 'center))
;
;(defun svg-progress-count (value)
;  (let* ((seq (mapcar #'string-to-number (split-string value "/")))
;         (count (float (car seq)))
;         (total (float (cadr seq))))
;  (svg-image (svg-lib-concat
;              (svg-lib-progress-bar (/ count total) nil
;                                    :margin 0 :stroke 2 :radius 3 :padding 2 :width 11)
;              (svg-lib-tag value nil
;                           :stroke 0 :margin 0)) :ascent 'center)))
;
;;Setting my own face
;;(defface myface '((t (:inherit org-todo) :height 1.0 :background "#957DAD")) "Face for todo")
;(defface pastelpurple '((t (:inherit org-todo :height 2.0 :foreground "#957DAD"))) "Face for todo")
;(defface pastelgreen '((t (:inherit org-todo :height 2.0 :foreground "#B0EB93"))) "Face for WIP")
;(defface pastelblue '((t (:inherit org-todo :height 2.0 :foreground "#B3E3DA"))) "Face for DONE")
;(defface pastelred '((t (:inherit org-todo :height 2.0 :foreground "#F98284"))) "Face for tags")
;(defface pastelpink '((t (:inherit org-todo :height 2.0 :foreground "#FEAAE4"))) "Face for Date")
;
;
;(setq svg-tag-tags
;      `(
;        ;; Org tags :THIS:
;        ("\\(:[A-Za-z0-9]+:\\)" . ((lambda (tag) (svg-tag-make tag :beg 1 :end -1 :inverse t :face 'pastelred))))
;	
;	;(1)
;	;("\([0-;9a-zA-Z]\)" . ((lambda (tag)
;        ;                        (svg-tag-make tag :beg 1 :end -1 :radius 12))))
;
;	;(09)
;        ;("\([0-9a-zA-Z][0-9a-zA-Z]\)" . ((lambda (tag)
;        ;                                   (svg-tag-make tag :beg 1 :end -1 :radius 8))))
;
;	;(007)
;        ;("\([0-9a-zA-Z][0-9a-zA-Z][0-9a-zA-Z]\)" . ((lambda (tag)
;        ;                                   (svg-tag-make tag :beg 1 :end -1 :radius 8))))
;
;	;Unsure?
;	;("|[0-9a-zA-Z- ]+?|" . ((lambda (tag)
;        ;                          (svg-tag-make tag :face 'font-lock-comment-face
;        ;                                        :margin 0 :beg 1 :end -1))))
;	
;        ;; Task priority [#a]
;        ("\\[#[a-zA-Z]\\]" . ( (lambda (tag)
;                              (svg-tag-make tag :face 'org-priority 
;                                            :beg 2 :end -1 :margin 0 :inverse t))))
;
;        ;; Progress [1/3] or [45%]
;        ("\\(\\[[0-9]\\{1,3\\}%\\]\\)" . ((lambda (tag)
;                                            (svg-progress-percent (substring tag 1 -2)))))
;        ("\\(\\[[0-9]+/[0-9]+\\]\\)" . ((lambda (tag)
;                                          (svg-progress-count (substring tag 1 -1)))))
;        
;        ;; Org TAGS
;        (":TODO:" . ((lambda (tag) (svg-tag-make "TODO" :inverse t :face 'pastelpurple))))
;        (":WIP:" . ((lambda (tag) (svg-tag-make "WIP" :inverse t :face 'pastelgreen))))
;        (":DONE:" . ((lambda (tag) (svg-tag-make "DONE" :inverse t :face 'pastelblue))))
;        (":NOTE:" . ((lambda (tag) (svg-tag-make "NOTE" :inverse t :face 'org-todo))))
;        ("SCHEDULED:" . ((lambda (tag) (svg-tag-make "SCHEDULED" :inverse t :face 'org-tag))))
;        ("DEADLINE:" . ((lambda (tag) (svg-tag-make "DEADLINE" :inverse t :face 'org-tag))))
;        ("+BEGIN_SRC" . ((lambda (tag) (svg-tag-make "BEGIN" :inverse t :face 'org-tag))))
;        ("+END_SRC" . ((lambda (tag) (svg-tag-make "END" :inverse t :face 'org-tag))))
;        ("+RESULTS:" . ((lambda (tag) (svg-tag-make "RESULTS" :inverse t :face 'org-tag))))
;        ("+title" . ((lambda (tag) (svg-tag-make "TITLE" :inverse t :face 'org-tag))))
;        ("+BEGIN:" . ((lambda (tag) (svg-tag-make "BEGIN" :inverse t :face 'org-tag))))
;        ("+CAPTION:" . ((lambda (tag) (svg-tag-make "CAPTION" :inverse t :face 'org-tag))))
;        ("+END:" . ((lambda (tag) (svg-tag-make "END" :inverse t :face 'org-tag))))
;	(":X" . ((lambda (tag) (svg-tag-make "[X]" :inverse t :face 'org-todo))))
;        (":-" . ((lambda (tag) (svg-tag-make "[-]" :inverse t :face 'org-tag))))
;	
;        ;; Citation of the form [cite:@Knuth:1984] 
;        ("\\(\\[cite:@[A-Za-z]+:\\)" . ((lambda (tag)
;                                          (svg-tag-make tag
;                                                        :inverse t
;                                                        :beg 7 :end -1
;                                                        :crop-right t))))
;        ("\\[cite:@[A-Za-z]+:\\([0-9]+\\]\\)" . ((lambda (tag)
;                                                (svg-tag-make tag
;                                                              :end -1
;                                                              :crop-left t))))
;
;
;	;;; Works for stuff like :XXX|YYY:
;	("\\(:[A-Z]+\\)\|[a-zA-Z#0-9]+:" . ((lambda (tag)
;                                           (svg-tag-make tag :beg 1 :inverse t
;                                                          :margin 0 :crop-right t))))        
;
;        (":[A-Z]+\\(\|[a-zA-Z#0-9]+:\\)" . ((lambda (tag)
;                                           (svg-tag-make tag :beg 1 :end -1
;                                                         :margin 0 :crop-left t))))
;	;; Active date (with or without day name, with or without time)
;        (,(format "\\(<%s>\\)" date-re) .
;         ((lambda (tag)
;            (svg-tag-make tag :beg 1 :end -1 :margin 0 :face 'pastelpink :inverse t))))
;        (,(format "\\(<%s \\)%s>" date-re day-time-re) .
;         ((lambda (tag)
;            (svg-tag-make tag :beg 1 :inverse t :crop-right t :margin 0 :face 'pastelpink))))
;        (,(format "<%s \\(%s>\\)" date-re day-time-re) .
;         ((lambda (tag)
;            (svg-tag-make tag :end -1 :inverse nil :crop-left t :margin 0 :face 'pastelpink))))))
;
;        ;; Inactive date  (with or without day name, with or without time)
;         ;(,(format "\\(\\[%s\\]\\)" date-re) .
;         ; ((lambda (tag)
;         ;    (svg-tag-make tag :beg 1 :end -1 :margin 0 :face 'org-date))))
;         ;(,(format "\\(\\[%s \\)%s\\]" date-re day-time-re) .
;         ; ((lambda (tag)
;         ;    (svg-tag-make tag :beg 1 :inverse nil :crop-right t :margin 0 :face 'org-date))))
;         ;(,(format "\\[%s \\(%s\\]\\)" date-re day-time-re) .
;         ; ((lambda (tag)
;         ;    (svg-tag-make tag :end -1 :inverse t :crop-left t :margin 0 :face 'org-date))))))
;
;
;;(setq svg-tag-tags
;;      '(
;;       (":TODO:" . ((lambda (tag) (svg-tag-make "TODO" :inverse t))))
;;       (":WIP:" . ((lambda (tag) (svg-tag-make "WIP" :inverse t))))
;;       (":DONE:" . ((lambda (tag) (svg-tag-make "DONE" :inverse t))))
;;       (":NOTE:" . ((lambda (tag) (svg-tag-make "NOTE" :inverse t))))
;;       ))
;
;(require 'svg-tag-mode)
;(global-svg-tag-mode 1)
;(use-package svg-lib)
;;(svg-lib-mode 1)
;
;;(minibuffer-header-mode 1)
;
;;(svg-lib)
;;(global-svg-tag-mode t)
;
;;; Ivy and Orderless
;(ivy-mode 1)
;
;(use-package orderless
;  :ensure t
;  :custom
;  (completion-styles '(orderless basic))
;  (completion-category-overrides '((file (styles basic partial-completion)))))
;
;(setq ivy-re-builders-alist '((t . orderless-ivy-re-builder)))
;(add-to-list 'ivy-highlight-functions-alist '(orderless-ivy-re-builder . orderless-ivy-highlight))
;
;;;;;;;;;;;;;;;;;;;;tabs;;;;;;;;;;;;;;;;;;;
;(use-package centaur-tabs
;  :demand
;  :config
;  (centaur-tabs-mode t)
;  :bind
;  ("C-<prior>" . centaur-tabs-backward)
;  ("C-<next>" . centaur-tabs-forward))
;(centaur-tabs-headline-match)
;(setq centaur-tabs-style "slant")
;(setq centaur-tabs-height 12)
;(setq centaur-tabs-set-icons t)
;(setq centaur-tabs-set-bar 'under)
;(setq centaur-tabs-close-button "X")
;(setq centaur-tabs-set-modified-marker t)
;(setq centaur-tabs-modified-marker "*")
;
;
;;;All-The-Icons
;(when (display-graphic-p)
;  (require 'all-the-icons))
;
;;; lsp extras
;(setq lsp-ui-sideline-enable t
;      lsp-ui-sideline-show-symbol t
;      lsp-ui-sideline-show-hover t
;      lsp-ui-sideline-show-flycheck t
;      lsp-ui-sideline-show-code-actions nil
;      lsp-ui-sideline-show-diagnostics nil)
;
;
;(require 'company)
;;(push 'company-lsp company-backends)
;;;(use-package lsp-mode
;;;  :commands (lsp lsp-deferred)
;;;  :init
;;;  (add-to-list 'company-backends 'company-capf)
;;;  (setq lsp-keymap-prefix "C-c l")
;;;  :config
;;;  (lsp-enable-which-key-integration t)
;;;  (setq lsp-ui-doc-enable nil)
;;;  :hook
;;;  ((go-mode) . lsp))
;
;(use-package lsp-mode
;  :hook ((lsp-mode . lsp-enable-which-key-integration))
;  :config
;  (setq
;   lsp-enable-file-watchers nil
;   lsp-headerline-breadcrumb-enable nil
;   )
;
;  ;; Performance tweaks, see
;  ;; https://github.com/emacs-lsp/lsp-mode#performance
;  (setq gc-cons-threshold 100000000)
;  (setq read-process-output-max (* 1024 1024)) ;; 1mb
;  (setq lsp-idle-delay 0.500)
;  )
;
;;(use-package lsp-mode
;;  :straight t
;;  :init (add-to-list 'company-backends 'company-capf)
;;  :config
;;  (setq lsp-ui-doc-enable nil)
;;  :bind ("C-c d d" . 'lsp-ui-doc-show)
;;  :bind ("C-c d h" . 'lsp-ui-doc-hide)
;;  :commands lsp)
;
;;(require 'lsp-java)
;;(add-hook 'java-mode-hook #'lsp)
;
;;;(condition-case nil
;;;    (require 'use-package)
;;;  (file-error
;;;   (require 'package)
;;;   (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
;;;   (package-initialize)
;;;   (package-refresh-contents)
;;;   (package-install 'use-package)
;;;   (setq use-package-always-ensure t)
;;;   (require 'use-package)))
;;;
;(use-package projectile)
;(use-package flycheck :ensure t :init (global-flycheck-mode))
;(use-package yasnippet :config (yas-global-mode))
;;(use-package lsp-mode :hook ((lsp-mode . lsp-enable-which-key-integration)))
;(use-package lsp-ui)
;(use-package which-key :config (which-key-mode))
;;(use-package lsp-java :config (add-hook 'java-mode-hook 'lsp))
;(use-package dap-mode :after lsp-mode :config (dap-auto-configure-mode))
;;(use-package dap-java :ensure nil)
;;;;;;;auto-correct;;;;;;;;;;;;;;;;;;;
;
;
;(require 'smex)
;(global-set-key (kbd "M-x") 'smex)
;
;
;;(toggle-menu-bar-mode-from-frame 1)
;;(toggle-frame-fullscreen)
;
;;;;;;; changing default font to Hacker
;;(set-face-attribute 'default nil :family "Iosevka")
;					;(set-frame-font "Hacker 14" nil t)
;;Works with Options->Set Default Font
;(set-face-attribute 'default nil :family "Iosevka")
;;(set-frame-font "Iosevka 18" nil t)
;
;;;Getting rid of line wrapping.
;(set-default 'truncate-lines t)
;
;;;;;;;;;;;;;;;; turn off the beeping ;;;;;;;;;;;
;(setq visible-bell 1)
;
;(global-set-key (kbd "C-s") 'swiper)
;(global-set-key (kbd "s-f") 'toggle-frame-fullscreen)
;(global-set-key (kbd "s-b") 'eval-buffer)
;;(global-set-key (kbd "C-x C-p") 'package-install)
;(global-linum-mode t)
;(global-set-key (kbd "C-x i") (lambda () (interactive)(find-file "/home/hank/.emacs")))
;
;(dogears-mode 1)
;(global-set-key (kbd "C-x x") 'dogears-list)
;
;(plist-put org-format-latex-options :scale 1.5)
;
;;;;; Golden Ratio ;;;;;
;; https://github.com/roman/golden-ratio.el
;(golden-ratio-mode 1)
;
;;; Org Babel
;;(load-library 'ob-rust)
;(org-babel-do-load-languages
; 'org-babel-load-languages
; '((python . t)))
;;   (octave . t)
;;   (rust . t)))
;(add-hook 'after-init-hook 'global-company-mode)
;;; Tabnine for code completion, download with M-x company-tabnine-install-binary after install
;(use-package company-tabnine :ensure t)
;(add-to-list 'company-backends #'company-tabnine)
;;; Trigger completion immediately.
;(setq company-idle-delay 0)
;
;;; Number the candidates (use M-1, M-2 etc to select completions).
;(setq company-show-numbers t)
;
;;;;LSP(Eglot) Stuff
;;; Optional: load other packages before eglot to enable eglot integrations.
;(require 'company)
;(require 'yasnippet)
;
;(require 'go-mode)
;
;;; Elpy
;(use-package elpy
;  :ensure t
;  :init
;  (elpy-enable))
;
;
;;; Perspective
;(require 'perspective)
;(global-set-key (kbd "C-x C-b") 'persp-list-buffers)
;(global-set-key (kbd "C-x C-p") 'persp-switch)
;(customize-set-variable 'persp-mode-prefix-key (kbd "C-c M-p"))
;(persp-mode)
;
;;;;;;;; Marginalia
;(marginalia-mode 1)
;
;;; Doom Modeline (never thought I'd stop using nano-modeline... :'( )
;(use-package doom-modeline
;  :ensure t
;  :init (doom-modeline-mode 1))
;
;;; Centered Cursor Mode
;(use-package centered-cursor-mode
;  :demand
;  :config
;  ;; Optional, enables centered-cursor-mode in all buffers.
;  (global-centered-cursor-mode))
;
;;;; Electric-pair mode (Matches parens and brackets)
;(electric-pair-mode 1)
;
;;; Topsy (Top bar shows function you're currently in)
;;;(topsy-mode 1)
;;(which-function-mode 1)
;(which-function-mode)
;
;(setq mode-line-misc-info (delete (assoc 'which-func-mode
;                                      mode-line-misc-info) mode-line-misc-info)
;      which-func-header-line-format '(which-func-mode ("" which-func-format)))
;(defadvice which-func-ff-hook (after header-line activate)
;  (when which-func-mode
;    (setq mode-line-misc-info (delete (assoc 'which-func-mode
;                                          mode-line-misc-info) mode-line-misc-info)
;          header-line-format which-func-header-line-format)))
;
;;; Dots for whitespaces (gotta figure out how to turn of the $ though, might just need to fork it)
;;; Also need to get of dots between spaces, only want it for the front of the line
;;;(require 'leerzeichen-mode)
;;;(leerzeichen-mode 1)
;;; What I want is a whitespace mode that shows whitespaces before code and nothing after
;(setq whitespace-line-column 1000)
;
;;; Rainbow delimiter mode changes colors of delimites to match their partner
;(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
;
;;; Line-reminder-mode, basically shows what you've changed in your current session
;(require 'indicators)
;(use-package line-reminder
;  :ensure t
;  :init
;  (setq line-reminder-show-option 'indicators)
;  ;; Customize the modified sign.
;  ;;(setq line-reminder-modified-sign "▐")  ; not needed
;  ;;Customize the saved sign.  
;  ;;(setq line-reminder-saved-sign "▐")  ; not needed
;  ;;Customize string on the right/left side of the line number.
;  (setq line-reminder-linum-left-string "")
;  (setq line-reminder-linum-right-string " ")
;  :config
;  (add-hook 'prog-mode-hook
;            (function
;             (lambda ()
;               (line-reminder-mode t)
;               (setq line-reminder-show-option 'indicators)))))
;(global-line-reminder-mode t)
;
;
;;; Origami Mode (Used for folding code)
;(global-origami-mode 1)
;(global-set-key (kbd "C-c C-c") 'origami-close-all-nodes)
;(global-set-key (kbd "C-c C-o") 'origami-open-all-nodes)
;(global-set-key (kbd "C-c c") 'origami-close-node-recursively)
;(global-set-key (kbd "C-c o") 'origami-open-node-recursively)
;
;;; JS development
;(use-package frontside-javascript
;  :init (frontside-javascript))
;
;;; Tree-sitter and tree-sitter-langs
;(require 'tree-sitter)
;(require 'tree-sitter-langs)
;
;;; Turns on tree-sitter-mode globaly and turns on the highlighting for it
;(global-tree-sitter-mode)
;(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)
;;; To check supported major modes run tree-sitter-major-mode-language-alist
;
;(tree-sitter-require 'rust)
;(tree-sitter-require 'python)
;(tree-sitter-require 'java)
;(tree-sitter-require 'javascript)
;
;
;;; :IMPORTANT: to view AST run tree-sitter-debug-mode
;
;;; :TODO: Work with tree queries with https://emacs-tree-sitter.github.io/getting-started/#play-around-with-tree-queries
;;; run tree-sitter-query-builder and use an example from below
;; Python:
;; (class_definition (identifier) @class)
;; (function_definition (identifier) @func)
;
;; Rust 
;; (function_item (identifier) @func)
;; (impl_item (type_identifier) @impl)
;
;; Javascript
;; (function_declaration (identifier) @func)
;; (variable_declarator (identifier) @var)
;
;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(catppuccin-latte))
 '(custom-safe-themes
   '("2af6d337981b88f85980124e47e11cbff819200bba92648f59154a6ff35a7801" "f5b179094d5d1172263af198f315ae85f49bfcd4660f49366f1718ee0e63129d" "bdf2b5b617ea284bf2635f28509b3a5ce3fb1ed82cac00196a71cd8403fa3e60" "798d594c14cfb510a09523dcab25a32dc543d68b04acfa29702b533911094198" "98fada4d13bcf1ff3a50fceb3ab1fea8619564bb01a8f744e5d22e8210bfff7b" "b9e9ba5aeedcc5ba8be99f1cc9301f6679912910ff92fdf7980929c2fc83ab4d" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "0c6471dc1af916bfe2493300ddfec2d11c87e09d364e24b659a3c3de555c9e3d" "f0eb51d80f73b247eb03ab216f94e9f86177863fb7e48b44aacaddbfe3357cf1" "db5b906ccc66db25ccd23fc531a213a1afb500d717125d526d8ff67df768f2fc" default))
 '(display-time-mode t)
 '(elfeed-feeds
   '("https://daverupert.com/atom.xml" "https://opensource.com/feed" "https://stackabuse.com/rss/" "https://godotengine.org/rss.xml"
     ("https://ooohspooky.libsyn.com/rss" AdamKnox)
     ("https://godotengine.org/rss.xml" Godot)
     ("https://stackabuse.com/rss/" StackAbuse)
     ("https://opensource.com/feed" OpenSource)
     ("https://daverupert.com/atom.xml" DaveRupert)
     ("https://feed.podbean.com/mateitsasurething/feed.xml" Wolfe)
     ("https://www.omnycontent.com/d/playlist/e73c998e-6e60-432f-8610-ae210140c5b1/0c6fc944-eaa5-4022-8e77-ae3300346d76/45a735b6-e1b8-49f9-a595-ae3300346d84/podcast.rss" Scrubs)))
 '(line-number-mode nil)
 '(mode-line-compact 'long)
 '(org-agenda-files nil)
 '(package-selected-packages
   '(org-superstar tree-sitter-langs tree-sitter orderless origami line-reminder rainbow-delimiters leerzeichen topsy buttercup frontside-javascript doom-modeline flycheck format-all company-tabnine imenu-extra imenu-anywhere persp-projectile treemacs-perspective marginalia centered-cursor-mode imenu-list which-key lsp-latex perspective lsp-mode all-the-icons-gnus vundo undo-tree company-go go-mode htmlize golden-ratio rustic elpy cl-libify autothemer multiple-cursors ob-sagemath ob-rust rust-mode org-fragtog csv-mode openwith vterm dogears minimap counsel-projectile counsel pdf-tools page-break-lines lsp-ivy lsp-ui kind-icon all-the-icons-dired all-the-icons dirvish yasnippet-snippets yasnippet org-hyperscheduler auto-correct meow magit hyperbole dashboard writeroom-mode smart-mode-line battery-notifier osm svg-tag-mode svg-lib unicode-fonts emojify ivy-emoji display-wttr celestial-mode-line elfeed-org elfeed-goodies all-the-icons-ivy mu4e-marker-icons ivy-emms emms elfeed-dashboard elfeed mu4e-alert mu4e-views dtache nano-modeline nano-sidebar auto-complete org-roam-ui smex all-the-icons-ivy-rich centaur-tabs use-package quelpa-use-package org-roam battle-haxe projectile swiper nano-theme ivy))
 '(tool-bar-mode nil)
 '(widget-image-enable nil))
`(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
'(default
((t
(:family "Iosevka" :foundry "UKWN" :slant normal :weight normal :height 143 :width normal)))))
(put 'scroll-left 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Iosevka" :foundry "UKWN" :slant normal :weight normal :height 143 :width normal))))
 '(line-reminder-modified-sign-face ((t (:foreground "#DDE165"))))
 '(whitespace-empty ((t nil)))
 '(whitespace-hspace ((t nil)))
 '(whitespace-newline ((t nil)))
 '(whitespace-space ((t nil)))
 '(whitespace-trailing ((t nil))))
